---
id: php-info
title: PHP
subskills:
  - Development
---
It isn't fancy. It isn't what "all the cool kids are using". But you know what PHP is? Dependable. That's what I look for in my server-side tools.

These are the same reasons why I use MySQL for all my database needs.
