---
id: develop-info
title: Front-end & Back-end Development
---
What started as a basic working knowledge of HTML and CSS, has morphed into something else entirely.

I'm well versed in JavaScript and PHP, and have a working knowledge of bash, SQL, and Python. 

With a strong foundation in the underlying languages, I can skillfully use any common framework built on top. For JavaScript, I like working with jQuery and React. When it comes to PHP, WordPress is my preferred CMS.
