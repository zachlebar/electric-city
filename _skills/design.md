---
id: design-info
title: Web & Graphic Design
---
Before anything else, I'm a designer. It informs everything I do.

I believe being a designer means you think about how the thing works, not just how it looks. You examine it from every angle, peer into the details, and then let that knowledge inform your every decision.

That's how to create something truly well-designed.
