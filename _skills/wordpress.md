---
id: wp-info
title: WordPress
subskills:
  - Design • Development
  - Custom Themes
  - WooCommerce • Sensei
---
I love WordPress. It's a fabulous tool with an incredible community. I specialize in custom theme development, and have experience integrating with popular plugins like WooCommerce and Sensei.

I also had the honor of being the keynote speaker at the first ever WordCamp Scranton in 2015.

<h4><a href="http://wordpress.tv/2015/08/03/zach-lebar-keynote-coming-back-to-wordpress/">You can watch a video of my talk here &rarr;</a></h4>
