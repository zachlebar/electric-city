---
id: html-info
title: HTML5 • CSS 3
subskills:
  - Development
---
Every site I build is written in modern, standards-compliant HTML5 and CSS3 and I'm a firm believer in progressive enhancement.
