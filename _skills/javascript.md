---
id: js-info
title: JavaScript
subskills:
  - Development
---
I think JavaScript is like hot sauce.

When it's used skillfully it adds an exciting pop, but you don't need it in everything, and a little goes a long way.
