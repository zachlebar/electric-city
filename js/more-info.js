//
// ZACH'S MAGICAL MORE INFO SCRIPT
// for
// ZACHLEBAR.COM
//
//

jQuery(document).ready(function($) {

  var modal = '<div class="modal"><a href="#" class="close-btn">X</a><section class="container"></section></div>';
  $("body").prepend(modal);
  $("article.skill").hide();
  $("article.project").hide();

  $("a.more-info").click(function(e) {
    var targetInfo = $(this).attr("href");

    $(".modal .container").html( $(targetInfo) );

    $(".modal").fadeIn("fast", function() {
      $(targetInfo).show();
    });

    e.preventDefault();
  });



  $(".close-btn").on("click", function(e) {

    $(".modal").fadeOut("fast");
    var targetContent = $(".modal .container").html();

    $(targetContent).appendTo("body").hide();

    $(".modal .container").html("");

    e.preventDefault();
  });

});
