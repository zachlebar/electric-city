---
id: svenson-info
title: Svenson Industries
agency: Apoterra Design
type: Design
images:
  - /img/svenson-3.jpg
  - /img/svenson-1.jpg
  - /img/svenson-2.jpg
---
A concept design done for Apoterra Design. The brief was to create a logo and letterhead for a fictional industrial company. They were looking for something that was strong and corporate, but different than your average monochromatic designs.
