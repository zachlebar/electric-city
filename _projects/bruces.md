---
id: bruce-info
title: Roofing by Bruce
type: Design
images:
  - /img/bruce-3.jpg
  - /img/bruce-2.jpg
  - /img/bruce-4.jpg
---
Roofing by Bruce is a family-owned and operated roofing and chimney company. They were looking for a modern interpretation of a retro look, inspired by vintage ads from the 1950s and 60s.
