---
id: beer-info
title: Blizzard Rock Brewery
agency: Apoterra Design
type: Design
images:
  - /img/beer-1.jpg
  - /img/beer-2.jpg
  - /img/beer-3.jpg
---
A concept design done for Apoterra Design. The brief was to create a logo and poster for a fictional microbrewery. Something you might see hanging up in a local bar, advertising their brand. A design that's a bit retro without being too kitschy.
