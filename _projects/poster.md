---
id: poster-info
title: Vandalay Investments
agency: Apoterra Design
type: Design
images:
  - /img/poster-1.jpg
  - /img/poster-3.jpg
  - /img/poster-2.jpg
---
A concept design done for Apoterra Design. The brief was to create a logo and poster for a fictional investment company. Something you might see hanging up in their waiting room, or used as a bus ad.
