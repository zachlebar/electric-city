---
id: greenridge-info
title: Green Ridge Academy
agency: Apoterra Design
type: Design
images:
  - /img/greenridge-2.jpg
  - /img/greenridge-1.jpg
  - /img/greenridge-3.jpg
---
A concept design done for Apoterra Design. The brief was to create a flexible brand for a fictional private school. They were looking for something that wasn't too traditional or too modern. Something that conveyed quality and determination.
