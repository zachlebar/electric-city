---
id: axelson-info
title: Axelson Aviation
agency: Apoterra Design
type: Design
images:
  - /img/axelson-1.jpg
  - /img/axelson-2.jpg
---
A concept design done for Apoterra Design. The brief was to create a clean and modern logo design for a fictional airline company. The mark needed to be usable at small sizes and across a wide range of applications.
