---
id: juniper-info
title: Juniper Creek
agency: Apoterra Design
type: Design
images:
  - /img/juniper-2.jpg
  - /img/juniper-1.jpg
  - /img/juniper-3.jpg
---
A concept design done for Apoterra Design. The brief was to create a brand for a Bed and Breakfast that also had a spa and restaurant on the premises. The base brand of "Juniper Creek" needed to pair with all the different sub-brands and accommodate a wide variety of use cases.
