---
id: tbfs-info
title: That Butterfly Feeling
type: Design
images:
  - /img/tbfs.jpg
---
That Butterfly Feeling makes handcrafted jewelry with an emphasis on natural materials and gem stones. They were looking for a simple mark that walked the line between elegance and whimsy.
