---
id: ducklings-info
title: Little Ducklings Daycare
agency: Apoterra Design
type: Design
images:
  - /img/ducklings-3.jpg
  - /img/ducklings-2.jpg
  - /img/ducklings-1.jpg
---
A concept design done for Apoterra Design. The brief was to create a logo for a fictional daycare. They were looking for something modern, but still conveyed character. A mark flexible and simple, with a dash of whimsy.
