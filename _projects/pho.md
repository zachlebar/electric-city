---
id: pho-info
title: Twenty Pho Seven
agency: Apoterra Design
type: Design
images:
  - /img/pho-1.jpg
  - /img/pho-2.jpg
  - /img/pho-3.jpg
---
A concept design done for Apoterra Design. The brief was to create a logo for a small franchise of fictional Vietnamese restaurants, and then design a door hanger menu featuring that mark for theoretical marketing purposes.
