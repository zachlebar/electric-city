---
id: adoption-info
title: Adoption Information Services
agency: Hire Jordan Smith
type: Design
images:
  - /img/adoption-1.jpg
  - /img/adoption-2.jpg
  - /img/adoption-3.jpg
---
A site designed to provide individuals on both sides of the adoption process with valuable information and resources. Adoption Information Services was looking for a site that was warm and inviting, featured prominent imagery, and had a touch of character.
