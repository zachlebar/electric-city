---
id: tunk-info
title: Tunkhannock Public Library
agency: Zen Design Firm
type: Development
demourl: http://tunkhannocklibrary.org/
images:
  - /img/tunk.jpg
---

Provided with a great design by Zen Design Firm, I built a custom WordPress theme for Tunkhannock Public Library. One piece that I'm especially proud of is the menu. Hidden behind an icon, the tiered menu allows for all the links the client requests without becoming overwhelming to the average visitor. The menu is fully responsive and works very well on small screens.
