---
id: southern-belle-info
title: Southern Belle Rentals
agency: Hire Jordan Smith
type: Design
images:
  - /img/southern-1.jpg
  - /img/southern-3.jpg
  - /img/southern-5.jpg
---
This was a real pleasure to work on. A site designed to showcase and sell rental properties in Savannah, GA and the surrounding area, Southern Belle lets the properties do the talking. We went with a monochromatic color scheme and let the phenomenal photography light up the page.
